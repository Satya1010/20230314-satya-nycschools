//
//  StringHelper.swift
//  Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation

extension String {
    
    /// Verifying wheather string contains given substring or not.Ignoring case senstive
    ///
    /// - Parameter find: input sub string
    /// - Returns: returns boolean value
    func containsIgnoringCase(find: String) -> Bool{
        return (self.range(of: find, options: .caseInsensitive) != nil)
    }
}
