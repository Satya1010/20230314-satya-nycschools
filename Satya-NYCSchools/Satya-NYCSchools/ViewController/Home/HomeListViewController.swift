//
//  HomeListViewController.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation
import UIKit
import MBProgressHUD


class HomeListViewController: UIViewController {
    //Tableview for displaying Schools in list
    @IBOutlet weak var tableView: UITableView!
    //Search bar to search with school name
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var homeViewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.title = "NYC Schools"
        
        prepareSchoolData()
        
    }
    
    /// Fetch all the schools data
    func prepareSchoolData() {
        // Show HUD
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        homeViewModel.fetchSchoolsData(completion: { [weak self] error in
            //Hide Hud
            progressHUD.hide(animated: true)
            if case .schoolApiError = error {
                //Display Schools API failure error
                self?.showMessageWith(title: "Error", message: error?.description ?? "Error while fetching API")
                return
            }
            
            self?.tableView.reloadData()
        })
    }
}

extension HomeListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeViewModel.schoolsData.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.identifier) as! SchoolTableViewCell
        cell.school = homeViewModel.schoolsData[indexPath.row]
        
        return cell
    }
}

extension HomeListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = homeViewModel.schoolsData[indexPath.row]
        self.performSegue(withIdentifier: "HomeToDetailSegue", sender: school)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let school = sender as? SchoolDataModel {
            guard let schoolSATDetails = homeViewModel.getSATDataWith(uniqueid: school.dbn ?? "") else { return
                
            }
            let satDetailsViewController = segue.destination as? SATDetailsViewController
            satDetailsViewController?.schoolSATModel = schoolSATDetails
            let dataModel = SATDataDisplayModel()
            dataModel.schoolLatitute = school.latitude
            dataModel.schoolLongitute = school.longitude
            dataModel.schoolOverView = school.overviewParagraph
            dataModel.phoneNumber = school.phoneNumber
            dataModel.primaryAddress1 = school.primaryAddressLine1
            satDetailsViewController?.satDataDisplayModel = dataModel
        }
    }
    
}

extension HomeListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text != homeViewModel.searchString {
            homeViewModel.searchSchool(string: searchBar.text ?? "")
            tableView.reloadData()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = homeViewModel.searchString
        searchBar.resignFirstResponder()
    }
}
